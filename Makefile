###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01e - Crazy Cat Lady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a Program to Print a sentence about Cats
###
### @author Leo Liang <leoliang@hawaii.edu> 
### @date   17/1/22 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = hello

all: $(TARGET)

hello: hello.c
	$(CC) $(CFLAGS) -o $(TARGET) hello.c

clean:
	rm -f $(TARGET) *.o

